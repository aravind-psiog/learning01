import json
import os
from typing import Optional

import aiofiles
import requests
from pydantic import BaseModel, condecimal

from fastapi import FastAPI, Form, Request, UploadFile
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from services import (checkmovie, createfile, deletemovie, fetchmovie,
                      updatefile)

BASE_PATH = os.getcwd()
TEMPLATES = Jinja2Templates(directory=f'{BASE_PATH}/static')
baseurl = "https://api.themoviedb.org/3/movie/now_playing"
apikey = "cc348043650d1146104248ee9c810fa6"


class MovieItems(BaseModel):
    title: str
    genre: list
    ratings: condecimal(decimal_places=2)
    year: int


class Movie(BaseModel):
    title: str


app = FastAPI()


@app.get("/")
async def root(request: Request):
    return TEMPLATES.TemplateResponse(
        "index.html",
        context={"request": request, "message": "Movie created successfully"},
    )


@app.post("/add_movie")
async def add_movie(params: MovieItems):
    json_item = jsonable_encoder(params)
    if checkmovie(json_item["title"]) is not True:
        createfile(json_item)
        return {"data": True,
                "message": f'Movie {json_item["title"]} created successfully'}
    return {"data": False, "message": "Movie already exists"}


@app.get("/get_movies")
async def get_movies(movie: str):
    if fetchmovie(movie) is not None:
        return {"data": True, "message": fetchmovie(movie)}
    return {"data": False, "message": "movie doesnt exists"}


@app.put("/update_movie")
async def update_movie(params: MovieItems):
    json_item = jsonable_encoder(params)
    if checkmovie(json_item["title"]) is True:
        updatefile(json_item)
        return {"data": True, "message": "Movie created successfully"}
    return {"data": False, "message": "Movie doesnot exist."}


@app.delete("/delete_movie")
async def delete_movie(title: Movie):
    json_item = jsonable_encoder(title)
    if fetchmovie((json_item["title"])) is not None:
        if deletemovie((json_item["title"])) is True:
            return {"data": True, "message": "Movie deleted successfully"}
        return {"data": False, "message": "Something went wrong"}
    return {"data": False, "message": "Movie doesnot exist."}


@app.get("/upload_image")
async def image_uploader(request: Request):
    return TEMPLATES.TemplateResponse(
        "uploader.html",
        context={"request": request, "message": "Movie created successfully"},
    )


@app.post("/upload")
async def upload(
                fname: Optional[str] = Form(...),
                upload: Optional[UploadFile] = Form(...)):
    if upload.content_type not in ["image/jpeg", "image/png"]:
        return {"data": False, "message": "Invalid format"}
    async with aiofiles.open(f"{BASE_PATH}/images/", 'w') as out_file:
        content = await upload.read()  # async read
        await out_file.write(content)  # async write
        return {"data": True, "message": "File saved"}


@app.get("/now_playing/{page}")
async def now_playing(request: Request, page: str):
    movies_list = []
    url = f"{baseurl}?api_key={apikey}&language=en-US&{page}"
    print(url)
    response1 = requests.request("GET", url)
    data1 = json.loads(response1.text)
    movies = data1["results"]
    for movie in movies:
        movies_list.append(movie["title"])
    return {"data": True, "message": movies_list}
