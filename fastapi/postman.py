import requests


def get():
    params = "tenet"
    url = f"http://127.0.0.1:8000/get_movies?movie={params}"

    data = {"title": "annihilation",
            "genre": ["scifi", "comedy"], "ratings": "6", "year": 2018}

    request_data = requests.get(url=url)
    data = request_data.json()
    return data


def post():
    url = "http://127.0.0.1:8000/add_movie"

    data = {"title": "annihilation",
            "genre": ["scifi", "comedy"], "ratings": "6", "year": 2018}

    request_data = requests.post(url=url, json=data)
    data = request_data.json()
    return data


def update():
    url = "http://127.0.0.1:8000/update_movie"

    data = {"title": "annihilation",
            "genre": ["scifi", "fiction"], "ratings": "6", "year": 2018}

    request_data = requests.put(url=url, json=data)
    data = request_data.json()
    return data


def delete():
    url = "http://127.0.0.1:8000/delete_movie"

    data = {"title": "annihilation"}

    request_data = requests.delete(url=url, json=data)
    data = request_data.json()
    return data
