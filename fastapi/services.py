import os
import json


BASE_PATH = os.getcwd()


def fetchmovie(movie):
    os.chdir(f'{BASE_PATH}/Database')
    check = os.listdir()
    print(check)
    if movie+".json" in set(check):
        f = open(movie+".json")
        details = json.load(f)
        return details
    else:
        return None


def checkmovie(movie):
    os.chdir(f'{BASE_PATH}/Database')
    check = os.listdir()
    print(movie)
    if movie+".json" in set(check):
        print(movie)
        return True
    return False


def createfile(data):
    os.chdir(f'{BASE_PATH}/Database')  # cd into database
    try:
        with open(data["title"]+".json", 'w') as file:
            json.dump(data, file)
        return True
    except Exception:
        return False


def updatefile(data):
    os.chdir(f'{BASE_PATH}/Database')  # cd into database
    print(os.getcwd())
    try:
        with open(data["title"]+".json", 'r+') as file:
            file.truncate(0)
            json.dump(data, file)
        return True
    except Exception:
        return False


def deletemovie(movie):
    try:
        os.remove(f'{BASE_PATH}/Database/{movie}.json')
        return True
    except Exception:
        return False


# def bucket(image,name):
#     CLIENT_ID = "6a961ecb9d667b3"
#     PATH = image

#     im = pyimgur.Imgur(CLIENT_ID)
#     uploaded_image = im.upload_image(PATH, title=name)
