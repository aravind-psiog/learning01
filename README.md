# Training @ Psiog
## _Programmatic view and concepts_


![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

This project covers learning module 01 - programmatic language view and module 02 - view and other concepts

- datatypes, loops, iterators, file handlers
- class, testing, code quality, type checking, dependency and workspace management
- documenting 

## Installation
clone the repo and install requirements by
```
cd learning01
pip install -r requirements.txt
```

requirements.txt contains so many dependencies because I was trying different packages for learning purpose.

## Explanation
### Testing
```test_first.py``` This file imports a module named ```postman.py``` from a package named as ```fastapi``` which test all the endpoints and creates a test report. The test report. There is already a test report ```FastApiReport.html``` . Run the test by

```
pytest
```
```pytest.ini``` contains the necessary custom configs which runs the test and generates the report.
### FastAPI
To run the FastAPI 
```
cd fastapi
uvicorn app:app --reload
```
Default hostname is ```http://localhost:8000``` and as it has inbuild documentation, ```http://localhost:8000/docs``` can be used to view/test the API's.
```app.py``` is the main app which has API functionalities. This file depends on a module named as ```services.py``` which makes file reading/writing.

### Folders

The folder ```Database``` acts like a local database which lies just to read/write when the API is called.

The folder ```static``` has the static html files used by FastAPI

```images``` is a another folder in which the uploaded image via FastAPI endpoint in saved.

### Code Quality
I have used linting tools like ```flake8``` which follows pep8 coding conventions and ```isort``` for module group management.

I have also used security vulnerability scanner ```bandit``` to check and warn about vulnerabilities i the code.
To check these run 
```
python -m flake8 script_name.py
python -m isort script_name.py
bandit script_name.py
```

## Things learned
There are number of things I have learned from this training. Some are them are

- Type convertors >> Implicit and Explicit.
- classes >> Inheritance which saves code and time by re-using it.
- pip freeze, virtual environment for module management.
- pytest >> fot unit test and validation which has vast number of features like grouping case, skipping cases, report generation etc.,
- FastAPI which is fast due to starlette a framework runs with ASGI giving it power of  asynchronous execution and pydantic a framework which makes data handling/validating easy. Has inbuild functionalities for login/auth like Oauth,JWT(not yet implemented)
- Linting tools like flake8,isort,bandit.
## Imrovements
- Test for positive and negative cases.
- Configuration file or environmental path for dynamic variables.
- json payload naming.
- Response codes.
The above changes will be reflected in 

https://bitbucket.org/aravind-psiog/learning01/src/tunings/tunnings/

## Reference

https://www.programiz.com/python-programming

https://testdriven.io/guides/complete-python/#Start

https://www.youtube.com/watch?v=_uQrJ0TkZlc

https://fastapi.tiangolo.com/

https://pycqa.github.io/isort/

https://pep8.org/
