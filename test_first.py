import pytest

from fastapi.postman import get, post, update, delete


@pytest.mark.group1
def test_get():
    check = get()
    if check["data"] is True or check["data"] is False:
        assert True
    else:
        assert False


@pytest.mark.group1
def test_post():
    check = post()
    if check["data"] is True or check["data"] is False:
        assert True
    else:
        assert False


@pytest.mark.group2
def test_update():
    check = update()
    if check["data"] is True or check["data"] is False:
        assert True
    else:
        assert False


@pytest.mark.group2
def test_delete():
    check = delete()
    if check["data"] is True or check["data"] is False:
        assert True
    else:
        assert False
